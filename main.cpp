#include <iostream>
#include <string>
#include <cmath>

using namespace std;

int main(){
    int a;
    int b;
    string operation;

    cout << "Enter a number!" << endl;

    cin >> a;

    cout << "Enter another number!" << endl;

    cin >> b;

    cout << "Enter an operation!" << endl;

    cin >> operation;

    if(operation == "+"){
        cout << "Answer: " << a+b << endl;
    }
    else if(operation == "-"){
        cout << "Answer: " << a-b << endl;
    }
    else if(operation == "*"){
        cout << "Answer: " << a*b << endl;
    }
    else if(operation == "/"){
        cout << "Answer: " << a/b << endl;
    }
    else if(operation == "%"){
        cout << "Answer: " << a%b << endl;
    }
    else if(operation == "^"){
        cout << "Answer: " << pow(a, b) << endl;
    }

    return 0;
}
